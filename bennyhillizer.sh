#!/bin/bash
theme=vZCCR-btg3o
target="$1"
outfile="$2"

usage () {
    echo "$0 <target-youtube-id> [outfile]" >&2
    exit 1
}

if [[ -z "$target" ]]; then
    usage
fi
outfile=${outfile:=out.mp4}

if [[ ! -n "$(find  . -maxdepth 1 -name '_theme.*' -print -quit)" ]]; then
    echo "Downloading theme ..." >&2
    yt-dlp -x -o "_theme.%(ext)s" -- "$theme"
fi
yt-dlp -o "_video.%(ext)s" -- "$target"
ffmpeg -i _video.* -i _theme.* -af apad -map 0:v:0 -map 1:a:0 -filter:v "setpts=0.5*PTS" -shortest "$outfile"
rm _video.*
